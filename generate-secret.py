from django.core.management.utils import get_random_secret_key

secret_key = get_random_secret_key()
print("generated secret key: " + secret_key)
print("writing secret key to file")
with open("secret_key", "w") as secret_key_file:
    secret_key_file.write(secret_key)
