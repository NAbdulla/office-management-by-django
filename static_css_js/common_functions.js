function getCharCount(str) {
    var cc = 0;
    for (let i = 0; i < str.length; i++) {
        if (str.charAt(i) != ' ' && str.charAt(i) != '\n') {
            cc++;
        }
    }
    return cc;
}