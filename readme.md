# Dev setup
Any of the following Python versions are supported. Use [pyenv](https://realpython.com/intro-to-pyenv) for better python version management.
 - 3.8
 - 3.7
 - 3.6

## Dependency installation
Run `pip install -r requirements.txt` to install the dependencies.

## Generate `secret_key` file
Run `python generate-secret.py` to create the `secret_key` file.

## Database preparation
Run `python manage.py migrate` to populate database with necessary data.

Now the web-app has only one user:
 - username: `admin`
 - password: `adminpass`

## Run
At the root of the project run `python manage.py runserver` or `python manage.py runserver 0.0.0.0:8000`.

## Database Backup
Create a directory named `backup_db_of_office_management_project` under the `$HOME` directory of the user. Then 
run the command to create backup when necessary: `python manage.py archive`. Use cron jobs to create backup on schedule.

# Run in dev container
It is also possible to run the project in dev container to start development without messing with the above configurations.
 - Install [Docker Desktop](https://www.docker.com/products/docker-desktop/)
 - Install [VS Code](https://code.visualstudio.com/download)
 - Install the [Dev Container Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) in VS Code
 - Open this project in VS Code
 - Accept prompt to open in Dev Container
 - While opening for the first time, it may take some time to open the container.
