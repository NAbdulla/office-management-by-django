"""Office_Management URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib.auth import views as auth_views

from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^index/$', index, name='index'),
    url(r'^view_all/$', view_all, name='view_all'),
    url(r'^accountant_home/$', accountant_home, name='accountant_home'),
    url(r'^admin_home/$', admin_home, name='admin_home'),
    url(r'^admin_actions/$', admin_actions, name='admin_actions'),
    url(r'^admin_accounts_manage', admin_accounts_manage, name='admin_accounts_manage'),
    url(r'^balance_check/$', balance_check, name='balance_check'),
    url(r'^change_pass/$', change_pass, name='change_pass'),
    url(r'^generate_report/$', generate_report, name='generate_report'),
    url(r'^accounts/profile/$', index, name='index'),
    url(r'^accounts/login/$', auth_views.LoginView.as_view(), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(next_page='index'), name='logout'),
    url(r'^recent5/$', recent_5_transactions, name='recent5'),
    url(r'^balances/$', balances, name='balances'),
    url(r'^blocked_users/$', blocked_users, name='blocked_users'),
    url(r'^about_devs/$', about_devs, name='about_devs'),
]
