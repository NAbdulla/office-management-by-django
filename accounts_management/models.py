from django.contrib.auth.models import User
from django.db import models


class Account(models.Model):
    account_id = models.CharField(primary_key=True, max_length=20, null=False)
    account_name = models.CharField(max_length=30, null=False)
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    need_student_id = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.account_name


class TransactionTypes(models.Model):
    id = models.CharField(max_length=20, primary_key=True)
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class UserType(models.Model):
    type_id = models.CharField(max_length=20, primary_key=True,
                               choices={('admin', 'Admin'), ('assistant_admin', "Admin's Assistant"),
                                        ('accountant', 'Accountant')})
    type_name = models.CharField(max_length=30)


class User2UserType(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    type = models.ForeignKey(UserType, on_delete=models.PROTECT)


class TransactionHistory(models.Model):
    account_id = models.ForeignKey(Account, on_delete=models.PROTECT)
    transaction_type = models.ForeignKey(TransactionTypes, on_delete=models.PROTECT)
    amount = models.DecimalField(max_digits=20, decimal_places=2, editable=False)
    description = models.TextField(max_length=5000, editable=True)
    date_time = models.DateTimeField(auto_now_add=True, editable=False)
    accountant = models.ForeignKey(User, on_delete=models.PROTECT)
    legal_doc_image = models.ImageField(upload_to='legal_docs/', null=True, blank=True)
    student_id = models.TextField(null=True, blank=True)
    balance_after_this_transaction = models.DecimalField(max_digits=20, decimal_places=2, editable=False, default=0.00)
