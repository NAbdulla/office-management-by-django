from django.apps import AppConfig


class AccountsManagementConfig(AppConfig):
    name = 'accounts_management'
