from django import forms

from .models import *


class AccountantForm(forms.ModelForm):
    account = forms.ModelChoiceField(required=True, widget=forms.Select,
                                     queryset=Account.objects.filter(is_active=True), empty_label="Select Account")
    amount = forms.DecimalField(required=True, min_value=0, max_digits=20, decimal_places=2)
    amount2 = forms.DecimalField(required=True, min_value=0, max_digits=20, decimal_places=2)
    tr_type = forms.ModelChoiceField(required=True, widget=forms.Select,
                                     queryset=TransactionTypes.objects.all(), empty_label="Select")
    desc = forms.CharField(required=True, widget=forms.Textarea(
        attrs={'rows': 3, 'cols': 30, 'maxlength': 5000, 'placeholder': "Description"}))

    class Meta:
        model = TransactionHistory
        fields = ['legal_doc_image', 'student_id']
        widgets = {
            'student_id': forms.TextInput(attrs={'placeholder': 'Student ID'})
        }
