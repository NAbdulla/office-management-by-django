# Generated by Django 2.2.3 on 2019-09-25 18:15

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('accounts_management', '0010_auto_20190925_2254'),
    ]

    operations = [
        migrations.AddField(
            model_name='transactionhistory',
            name='student_id',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='usertype',
            name='type_id',
            field=models.CharField(
                choices=[('assistant_admin', "Admin's Assistant"), ('accountant', 'Accountant'), ('admin', 'Admin')],
                max_length=20, primary_key=True, serialize=False),
        ),
    ]
