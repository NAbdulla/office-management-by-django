# Generated by Django 2.1.5 on 2019-05-17 09:33

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('accounts_management', '0003_auto_20190416_0049'),
    ]

    operations = [
        migrations.CreateModel(
            name='TransactionHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.DecimalField(decimal_places=2, editable=False, max_digits=20)),
                ('description', models.TextField(max_length=5000)),
                ('date_time', models.DateTimeField(auto_now_add=True)),
                ('account_id', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='accounts_management.Account')),
                ('accountant', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('transaction_type', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='accounts_management.TransactionTypes')),
            ],
        ),
        migrations.AlterField(
            model_name='user2usertype',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='accounts_management.UserType'),
        ),
        migrations.AlterField(
            model_name='user2usertype',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
    ]
