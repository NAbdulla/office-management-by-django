# Generated by Django 2.1.5 on 2019-05-17 12:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts_management', '0006_auto_20190517_1759'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transactionhistory',
            old_name='account',
            new_name='account_id',
        ),
        migrations.AlterField(
            model_name='usertype',
            name='type_id',
            field=models.CharField(choices=[('admin', 'Admin'), ('accountant', 'Accountant'), ('assistant_admin', "Admin's Assistant")], max_length=20, primary_key=True, serialize=False),
        ),
    ]
