from datetime import datetime
from datetime import timedelta
from decimal import Decimal

import pytz
from PIL import Image
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.db.models.query import *
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render

from .forms import *
from .models import *

in_recent = 5
max_doc_file_size = 10 * 1024 * 1024  # bytes = 10MB


@login_required
def index(request):
    check_is_active(request.user)
    user_type_id = request.user.user2usertype_set.all()[0].type.type_id
    if user_type_id == 'admin' or user_type_id == 'assistant_admin':
        return HttpResponseRedirect('/admin_home/')
    elif user_type_id == 'accountant':
        return HttpResponseRedirect('/accountant_home/')


@login_required
def accountant_home(request):
    check_is_active(request.user)
    user_type_id = get_user_type(request)
    if user_type_id == 'admin' or user_type_id == 'assistant_admin':
        return HttpResponseRedirect('/admin_home/')

    req_st_id = [account.account_id for account in Account.objects.filter(is_active=True).filter(need_student_id=True)]
    context = {'accform': AccountantForm(), 'status': None, 'count_range': [x for x in range(in_recent)],
               'mxf': max_doc_file_size, 'req_st_id_lst': req_st_id}

    if request.POST:
        tr_form = AccountantForm(request.POST)
        if tr_form.is_valid():
            account = None
            if Account.objects.filter(account_id=request.POST['account']):
                account = Account.objects.get(account_id=request.POST['account'])
            amount = float(request.POST['amount'])
            amount2 = float(request.POST['amount2'])

            student_id = None
            if request.POST['student_id']:
                student_id = request.POST['student_id']

            image = None
            if request.FILES['legal_doc_image']:
                image = request.FILES['legal_doc_image']
            else:
                context['status'] = 'No "Image of the Document" is found'
                tr_form.add_error('legal_doc_image', "No image found")

            if account is None or not account.is_active:
                context['status'] = "Account isn't exists or is disabled by admin."
            elif not is_image(image):
                context['status'] = 'Please insert a valid image file'
            elif image.size >= 10 * 1024 * 1024:  # 10*1024*1024 byte = 10 MB
                context['status'] = 'File size must be less than 10MB'
            elif amount <= 0:
                context['status'] = 'Amount must be positive number'
            elif amount != amount2:
                context['status'] = 'Both Amount must be same.'
            elif account in req_st_id and request.POST['tr_type'] == 'credit' and not student_id:
                context['status'] = 'Selected Account requires Student ID. But no Student ID is given'
            else:
                if request.POST['tr_type'] == 'debit':
                    if account.amount < amount:
                        context['status'] = 'Not enough balance in account.'
                    else:
                        with transaction.atomic():
                            account.amount -= Decimal(amount)
                            th = TransactionHistory(account_id=account,
                                                    transaction_type=TransactionTypes.objects.get(id='debit'),
                                                    amount=amount, description=request.POST['desc'],
                                                    accountant=request.user,
                                                    legal_doc_image=image,
                                                    student_id=student_id,
                                                    balance_after_this_transaction=account.amount)
                            account.save()
                            th.save()
                else:
                    with transaction.atomic():
                        account.amount += Decimal(amount)
                        th = TransactionHistory(account_id=account,
                                                transaction_type=TransactionTypes.objects.get(id='credit'),
                                                amount=amount, description=request.POST['desc'],
                                                accountant=request.user,
                                                legal_doc_image=image,
                                                student_id=student_id,
                                                balance_after_this_transaction=account.amount)
                        account.save()
                        th.save()
        else:
            for field, errors in tr_form.errors.items():
                for error in errors:
                    if context['status'] is None:
                        context['status'] = field + " - " + error
                    else:
                        context['status'] += '\n' + field + " - " + error
        if not context['status']:  # if there is not any error
            return HttpResponseRedirect('/accountant_home/')
    return render(request, 'accountant.html', context)


@login_required
def admin_home(request):
    check_is_active(request.user)
    if get_user_type(request) == 'accountant':
        return HttpResponseRedirect('/accountant_home/')

    acc_info = [{'name': acc.account_name, 'amount': acc.amount} for acc in Account.objects.filter(is_active=True)]
    context = {'is_admin': get_user_type(request) == 'admin', 'count_range': [x for x in range(in_recent)],
               'acc_info': acc_info}
    return render(request, 'admin.html', context=context)


@login_required
def admin_actions(request):
    check_is_active(request.user)
    if get_user_type(request) == 'accountant':
        return HttpResponseRedirect('/accountant_home/')
    elif get_user_type(request) == 'assistant_admin':
        return HttpResponseRedirect('/admin_home/')

    user_types = [UserType(type_id="", type_name="Select")]
    user_types += [ty for ty in filter(lambda t: t.type_id != 'admin', UserType.objects.all())]
    ctx = {'user_types': user_types, 'msg': None}

    if request.POST:
        if 'password2' in request.POST:  # new user add
            if 'username' in request.POST and 'password1' in request.POST and 'userType' in request.POST:
                username = request.POST['username']
                password1 = request.POST['password1']
                password2 = request.POST['password2']
                user_type = request.POST['userType']
                if password2 == password1:
                    if not User.objects.filter(username=username):  # username is not taken previously
                        tt = UserType.objects.filter(type_id=user_type)
                        if tt:
                            user = User.objects.create_user(username=username, password=password1)
                            u2u = User2UserType(user=user, type=tt[0])
                            user.save()
                            u2u.save(True)
                            ctx['msg'] = username + ' added'
                        else:
                            ctx['msg'] = 'Invalid user type'
                    else:
                        ctx['msg'] = 'username already exists, try another'
                else:
                    ctx['msg'] = 'The two password fields didn\'t match.'
                if not ctx['msg']:
                    return HttpResponseRedirect('/admin_actions/')
            else:
                ctx['msg'] = 'Username or Password or UserType is not given'
        else:  # user block option
            for username in request.POST:
                if User.objects.filter(username=username):
                    if User.objects.filter(username=username)[0].user2usertype_set.all()[0].type.type_id != 'admin':
                        user = User.objects.get(username=username)
                        user.is_active = False
                        user.save()
            return HttpResponseRedirect('/admin_actions/')

    users = []
    for u in User.objects.all():
        if u.user2usertype_set.all()[0].type.type_id != 'admin' and u.is_active:
            users.append({'username': u.username, 'type': u.user2usertype_set.all()[0].type.type_name})
    ctx['users'] = users
    return render(request, 'admin_actions.html', context=ctx)


@login_required
def admin_accounts_manage(request):
    check_is_active(request.user)
    if get_user_type(request) == 'accountant':
        return HttpResponseRedirect('/accountant_home/')
    elif get_user_type(request) == 'assistant_admin':
        return HttpResponseRedirect('/admin_home/')

    ctx = {'msg': None}
    if request.POST:
        ok = True
        if 'account_name' in request.POST:
            if 'account_name' not in request.POST or 'account_id' not in request.POST:
                ok = False
                ctx['msg'] = "Account Name or Account ID not posted."
            if ok:
                acc_name = request.POST['account_name']
                acc_id = request.POST['account_id']
                is_stu_req = False
                if 'is_student_id_required' in request.POST:
                    is_stu_req = True
                accs_nm = [a.account_name for a in Account.objects.all()]
                accs_id = [a.account_id for a in Account.objects.all()]
                if acc_name in accs_nm or acc_id in accs_id:
                    ctx['msg'] = "Account Name or Account ID already exists, may be disabled, try different Name and " \
                                 "ID. "
                else:
                    acc = Account.objects.create(account_name=acc_name,
                                                 account_id=acc_id,
                                                 need_student_id=is_stu_req,
                                                 amount=0.0)
                    acc.save()
                return HttpResponseRedirect('/admin_accounts_manage/')
        else:  # account status toggle
            for acc_id in request.POST:
                if Account.objects.filter(pk=acc_id):
                    acc = Account.objects.get(pk=acc_id)
                    acc.is_active = (not acc.is_active)
                    acc.save()
            return HttpResponseRedirect("/admin_accounts_manage/")
    accounts = [{'name': acc.account_name, 'id': acc.account_id, 'status': acc.is_active} for acc in
                Account.objects.all()]
    ctx['accounts'] = accounts
    return render(request, 'admin_accounts_manage.html', context=ctx)


@login_required
def blocked_users(request):
    check_is_active(request.user)
    if get_user_type(request) == 'accountant':
        return HttpResponseRedirect('/accountant_home/')
    elif get_user_type(request) == 'assistant_admin':
        return HttpResponseRedirect('/admin_home/')

    if request.POST:
        for username in request.POST:
            if User.objects.filter(username=username):
                if User.objects.filter(username=username)[0].user2usertype_set.all()[0].type.type_id != 'admin':
                    user = User.objects.get(username=username)
                    user.is_active = True
                    user.save()
        return HttpResponseRedirect('/admin_actions/')

    users = []
    for u in User.objects.all():
        if not u.is_active:
            users.append({'username': u.username, 'type': u.user2usertype_set.all()[0].type.type_name})
    ctx = {'users': users}
    return render(request, 'blocked_users.html', context=ctx)


@login_required
def balance_check(request):
    check_is_active(request.user)
    acc_info = [{'name': acc.account_name, 'amount': acc.amount} for acc in Account.objects.filter(is_active=True)]
    context = {'acc_info': acc_info}
    return render(request, 'balance_check.html', context=context)


@login_required
def change_pass(request):
    check_is_active(request.user)
    ctx = {}
    if request.POST and request.POST['old_pass']:
        if 'old_pass' not in request.POST:
            ctx['err'] = "Give old password"
        elif 'new_pass' not in request.POST:
            ctx['err'] = "Give new password"
        elif 'new_pass2' not in request.POST:
            ctx['err'] = "Give new password again"
        elif request.POST['new_pass'] != request.POST['new_pass2']:
            ctx['err'] = "New passwords don't match"
        else:
            cuser = authenticate(username=request.user.username, password=request.POST['old_pass'])
            if cuser is not None:
                u = User.objects.get(username=request.user.username)
                u.set_password(request.POST['new_pass'])
                u.save()
                ctx['info'] = 'Password changed successfully.'
                return HttpResponseRedirect('/change_pass/')
            else:
                ctx['err'] = "Old password may be incorrect"
    return render(request, 'change_pass.html', context=ctx)


@login_required
def view_all(request):
    check_is_active(request.user)

    account_list = [{'value': a.account_id, 'name': a.account_name} for a in Account.objects.all()]
    tr_types = [{'value': tr.id, 'name': tr.name} for tr in TransactionTypes.objects.all()]
    all_checked = True

    ctx = {'account_list': account_list, 'tr_types': tr_types}

    checked = []
    full_dscr = False
    show_accountant = False
    show_student_id = False
    page = 1

    rows = TransactionHistory.objects.all()

    if request.POST:
        all_checked = False
        page = None
        start = None
        end = None
        if 'page' not in request.POST:
            page = 1
        else:
            page = int(request.POST['page'])
        if 'from' in request.POST:
            start = request.POST['from']
        if 'to' in request.POST:
            end = request.POST['to']
        if start and end:
            start = datetime(year=int(start.split('-')[0]), month=int(start.split('-')[1]),
                             day=int(start.split('-')[2]), tzinfo=pytz.timezone(settings.TIME_ZONE))
            end = datetime(year=int(end.split('-')[0]), month=int(end.split('-')[1]), day=int(end.split('-')[2]),
                           tzinfo=pytz.timezone(settings.TIME_ZONE))

            ctx['start_date'] = start
            ctx['end_date'] = end

            end2 = end + timedelta(days=1)
            end2 = end2 - timedelta(seconds=1)

            rows = rows.filter(date_time__gte=start, date_time__lte=end2)

        for d in account_list:
            if d['value'] in request.POST and request.POST[d['value']] == 'on':
                checked.append(d['value'])
        rows = rows.filter(account_id_id__in=checked)

        for d in tr_types:
            if d['value'] in request.POST and request.POST[d['value']] == 'on':
                checked.append(d['value'])
        rows = rows.filter(transaction_type_id__in=checked)

        if 'full_dscr' in request.POST and request.POST['full_dscr'] == 'on':
            full_dscr = True
        if 'show_accountant' in request.POST and request.POST['show_accountant'] == 'on':
            show_accountant = True
        if 'show_student_id' in request.POST and request.POST['show_student_id'] == 'on':
            show_student_id = True

        trs = [{'dt': t.date_time, 'acc': t.account_id_id, 'amount': t.amount, 'type': t.transaction_type_id,
                'desc': t.description, 'user_id': t.accountant_id,
                'doc_url': t.legal_doc_image.url if t.legal_doc_image else "",
                'st_id': t.student_id}
               for t in rows.order_by('-date_time')]
    else:
        trs = [{'dt': t.date_time, 'acc': t.account_id_id, 'amount': t.amount, 'type': t.transaction_type_id,
                'desc': t.description, 'user_id': t.accountant_id,
                'doc_url': t.legal_doc_image.url if t.legal_doc_image else "",
                'st_id': t.student_id}
               for t in TransactionHistory.objects.all().order_by('-date_time')]

    ctx['all_checked'] = all_checked
    ctx['checked'] = checked
    ctx['full_dscr'] = full_dscr
    ctx['show_accountant'] = show_accountant
    ctx['show_student_id'] = show_student_id

    for t in trs:
        t['acc'] = Account.objects.get(account_id=t['acc']).account_name
        t['type'] = "Debit" if t['type'] == 'debit' else "Credit"
        t['desc'] = t['desc'] if full_dscr else t['desc'][:35]
        t['user_id'] = User.objects.get(id=t['user_id']).username

    per_page = 30
    tot = len(trs)
    pages = tot // per_page + int(tot % per_page != 0)
    page = min(page, pages)
    ctx['trs'] = trs[(page - 1) * per_page:min(page * per_page, tot)]
    ctx['first_ind'] = (page - 1) * per_page
    per_line = 30
    ctx['pages'] = pages
    ctx['per_line'] = per_line
    ctx['page_numbers'] = [(x + 1) % per_line for x in range(pages)]
    ctx['cur_page'] = page
    return render(request, 'view_all.html', context=ctx)


@login_required
def generate_report(request):
    check_is_active(request.user)

    account_list = [{'value': a.account_id, 'name': a.account_name} for a in Account.objects.all()]
    tr_types = [{'value': tr.id, 'name': tr.name} for tr in TransactionTypes.objects.all()]

    ctx = {'account_list': account_list, 'tr_types': tr_types, 'date_error': False}

    if request.POST:
        start = None
        end = None
        if 'from' not in request.POST:
            ctx['date_error'] = "Start date is not given"
        elif 'to' not in request.POST:
            ctx['date_error'] = "End date is not given"
        else:
            start = request.POST['from']
            end = request.POST['to']
        if start and end:
            report = {}
            checked = []
            show_accountant = False

            start = datetime(year=int(start.split('-')[0]), month=int(start.split('-')[1]),
                             day=int(start.split('-')[2]), tzinfo=pytz.timezone(settings.TIME_ZONE))
            end = datetime(year=int(end.split('-')[0]), month=int(end.split('-')[1]), day=int(end.split('-')[2]),
                           tzinfo=pytz.timezone(settings.TIME_ZONE))

            report['start_date'] = start
            report['end_date'] = end

            end2 = end + timedelta(days=1)
            end2 = end2 - timedelta(seconds=1)

            rows = TransactionHistory.objects.all()

            rows = rows.filter(date_time__gte=start, date_time__lte=end2)

            for d in account_list:
                if d['value'] in request.POST and request.POST[d['value']] == 'on':
                    checked.append(d['value'])
            rows = rows.filter(account_id_id__in=checked)

            for d in tr_types:
                if d['value'] in request.POST and request.POST[d['value']] == 'on':
                    checked.append(d['value'])
            rows = rows.filter(transaction_type_id__in=checked)

            if 'show_accountant' in request.POST and request.POST['show_accountant'] == 'on':
                show_accountant = True
            report['show_accountant'] = show_accountant

            rows.order_by('account_id_id', 'date_time')
            trs = [{'dt': t.date_time,
                    'acc': Account.objects.get(account_id=t.account_id_id).account_name,
                    'amount': t.amount,
                    'is_debit': t.transaction_type_id == 'debit',
                    'balance': t.balance_after_this_transaction,
                    'user_id': User.objects.get(id=t.accountant_id).username}
                   for t in rows]

            report['trs'] = trs
            report['user_type'] = request.user.user2usertype_set.all()[0].type.type_name
            return render(request, 'report.html', context=report)
        else:
            ctx['date_error'] = True
    return render(request, 'generate_report.html', context=ctx)


@login_required
def recent_5_transactions(request):
    check_is_active(request.user)

    trs = [{'acc': t.account_id_id, 'amount': t.amount, 'type': t.transaction_type_id}
           for t in TransactionHistory.objects.all().order_by('-date_time')[:in_recent]]
    for t in trs:
        t['acc'] = Account.objects.get(account_id=t['acc']).account_name
        t['type'] = "Debit" if t['type'] == 'debit' else "Credit"
    return JsonResponse(data=trs, safe=False)


@login_required
def balances(request):
    check_is_active(request.user)
    if get_user_type(request) == 'accountant':
        return HttpResponseRedirect('/accountant_home/')

    acc_info = [{'name': acc.account_name, 'amount': acc.amount} for acc in Account.objects.filter(is_active=True)]
    return JsonResponse(data=acc_info, safe=False)


def about_devs(request):
    is_logged_in = request.user.is_authenticated and request.user.is_active
    return render(request, 'about_devs.html', {'logged_in': is_logged_in})


def get_user_type(request):
    return request.user.user2usertype_set.all()[0].type.type_id


def check_is_active(user):
    if not user.is_active:
        return HttpResponseRedirect('/accounts/login/')


def is_image(image):
    try:
        Image.open(image)
        return True
    except IOError:
        return False
